package com.example.csp_6.instances;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.csp_6.Custcamera;
import com.example.csp_6.Dashboard;
import com.example.csp_6.DbHelper;
import com.example.csp_6.ListDataActivity;
import com.example.csp_6.ListReports;
import com.example.csp_6.MainActivity;
import com.example.csp_6.R;
import com.example.csp_6.Uploadreport;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DataAdapter extends ArrayAdapter {

    ProgressDialog progressDialog;
    DbHelper mDatabseHelper;

    public DataAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ReportHolder reportHolder;
        View row;
        row = convertView;

        if(row==null){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.view_adapter,parent,false);
            reportHolder = new ReportHolder();

            reportHolder.title = (TextView) row.findViewById(R.id.reptitle);
            reportHolder.image = (ImageView)row.findViewById(R.id.reportimage);
            reportHolder.description = (TextView)row.findViewById(R.id.repdescription);
            reportHolder.status = (TextView)row.findViewById(R.id.status);
            reportHolder.venue = (TextView)row.findViewById(R.id.venue);
            reportHolder.image = (ImageView)row.findViewById(R.id.reportimage);
            reportHolder.like = (Button)row.findViewById(R.id.like);
            reportHolder.upload = (Button)row.findViewById(R.id.upload);

            reportHolder.upload.setVisibility(View.INVISIBLE);
            reportHolder.like.setVisibility(View.INVISIBLE);

            row.setTag(reportHolder);
        }
        else{
            reportHolder = (ReportHolder)row.getTag();
        }

        final DataReps report = (DataReps) this.getItem(position);

        if(report.getcheckBlob()==true){
           // report.setcheckBlob(false);

            Bitmap bitmaplocal = BitmapFactory.decodeByteArray(report.getimgBlob(), 0, report.getimgBlob().length);
            reportHolder.image.setImageBitmap(bitmaplocal);

            reportHolder.upload.setVisibility(View.VISIBLE);

            reportHolder.upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // ImageUploadToServerFunction(view.getContext(),report.getReptitle());
                }
            });


        }
        /*else if(report.getlikec()==true){


            Picasso.get().load(report.getImage_url()).into(reportHolder.image);

            reportHolder.like.setVisibility(View.VISIBLE);

        }*/

        else{
            Picasso.get().load(report.getImage_url()).rotate(90).into(reportHolder.image);





            reportHolder.like.setVisibility(View.VISIBLE);


            reportHolder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Like like = new Like(view.getContext(),report.getReptitle());
                    like.execute();
                }
            });
        }

        reportHolder.title.setText(report.getReptitle());
        reportHolder.venue.setText(report.getCity_name());
        reportHolder.description.setText(report.getDescp());
        reportHolder.status.setText(report.getStatus());
        return row;
    }


    /*test*//*

    public void ImageUploadToServerFunction(Context ctx,String rep_id){






        ByteArrayOutputStream byteArrayOutputStreamObject,byteArrayOutputStreamObject2 ;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        byteArrayOutputStreamObject2 = new ByteArrayOutputStream();

        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
        Custcamera.bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamObject);
        Custcamera.bitmap2.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamObject2);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        byte[] byteArrayVar2 = byteArrayOutputStreamObject2.toByteArray();




        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        final String ConvertImage2 = Base64.encodeToString(byteArrayVar2, Base64.DEFAULT);

        // CaptureImageFromCamera.setVisibility(View.VISIBLE);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                // Showing progress dialog at image upload time.
                progressDialog = ProgressDialog.show(ctx,"Image is Uploading","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                //Toast.makeText(DataAdapter.this,string1,Toast.LENGTH_LONG).show();

                // Setting image as transparent after done uploading.
                //ImageViewHolder.setImageResource(android.R.color.transparent);

                if(string1.equalsIgnoreCase("Your Image Has Been Uploaded.")){

                    Custcamera.bitmap = null;
                    Custcamera.bitmap2 = null;
                    Custcamera.baseline=0;

                    Intent in = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(in);
                }


            }

            @Override
            protected String doInBackground(Void... params) {


                mDatabseHelper = new DbHelper(ctx);

                Cursor data= mDatabseHelper.upData(Integer.parseInt(rep_id));

                data.getString(1);


                DataAdapter.ImageProcessClass imageProcessClass = new DataAdapter.ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                //HashMapParams.put(ImageNameFieldOnServer, GetImageNameFromEditText);

                HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                HashMapParams.put(ImagePathFieldOnServer2, ConvertImage2);

                HashMapParams.put("lati", String.valueOf(lati));

                HashMapParams.put("longi", String.valueOf(longi));

                HashMapParams.put("baseline", String.valueOf(Custcamera.baseline));

                HashMapParams.put("focal_length", String.valueOf(focalLength));

                HashMapParams.put("ccdwidth", String.valueOf(size));

                HashMapParams.put("Emp_email", MainActivity.EmailHolder);

                HashMapParams.put("city", city);

                HashMapParams.put("street", area);

                HashMapParams.put("pincode", postalcode);

                HashMapParams.put("imgdes", GetImageNameFromEditText);

                //HashMapParams.put("empid", String.valueOf(width));

                // HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                String FinalData = imageProcessClass.ImageHttpRequest(ImageUploadPathOnSever, HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);
                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);
                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(new OutputStreamWriter(OutPutStream, "UTF-8"));
                bufferedWriterObject.write(bufferedWriterDataFN(PData));
                bufferedWriterObject.flush();
                bufferedWriterObject.close();
                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){
                        stringBuilder.append(RC2);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;
            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilderObject.append("&");
                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilderObject.append("=");
                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }


    //end test
*/



}

class ReportHolder{
    TextView repid;
    TextView title;
    TextView description;
    ImageView image;
    TextView status;
    TextView venue;

    Button like;
    Button upload;
}



class Like extends AsyncTask<String, Void, String> {

    private Context ctx;
    private String repid;

    Like(Context ctx,String repid){
        this.ctx = ctx;
        this.repid = repid;
    }
    private ProgressDialog progressDialog;




    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        if(progressDialog.isShowing()){
            progressDialog.cancel();
        }
        Toast.makeText(ctx,s,Toast.LENGTH_LONG).show();
        super.onPostExecute(s);
    }

    @Override
    protected String doInBackground(String... strings) {

        //Token token = new Token(ctx,null,null,1);
        String strurl = "http://mastekcsp.hostingerapp.com/Mastek/Basic/likes.php?email="+ MainActivity.EmailHolder +"&repid="+repid;

        try {
            URL url = new URL(strurl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);

            String data = URLEncoder.encode("email")+"="+URLEncoder.encode(MainActivity.EmailHolder);

            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
            bufferedWriter.write(data);

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine())!=null){
                stringBuilder.append(line);
            }

            return stringBuilder.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }
}



class Upload extends AsyncTask<String, Void, String> {

    private Context ctx;
    private String repid;

    Upload(Context ctx, String repid) {
        this.ctx = ctx;
        this.repid = repid;
    }

    private ProgressDialog progressDialog;


    @Override
    protected String doInBackground(String... strings) {
        return null;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        if (progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        Toast.makeText(ctx, s, Toast.LENGTH_LONG).show();
        super.onPostExecute(s);
    }



}


