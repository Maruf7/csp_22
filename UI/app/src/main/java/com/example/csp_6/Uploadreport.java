package com.example.csp_6;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.SizeF;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/*import com.android.internal.http.multipart.MultipartEntity;
import com.android.internal.http.multipart.Part;

import org.apache.http.entity.mime.content.FileBody;*/


import org.apache.http.HttpEntity;
/*import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/*import static com.example.csp_6.Custcamera.bitmap;
import static com.example.csp_6.Custcamera.bitmap2;*/
import static java.lang.Math.tan;


public class Uploadreport extends AppCompatActivity implements LocationListener {

    private static final String IMAGE_DIRECTORY = "/CustomImage";

    public TextView txt1, txt2, txt3, txt4;
    ImageView img1,img2;
    Camera mCamera;
    float focalLength;
    float horizontalViewAngle;
    float verticalViewAngle;
    float width;
    SizeF size;
    Button b3,imgclear;
    boolean check = true;

    LocationManager locationManager;
    double lati;
    double longi;
    ProgressDialog progressDialog;
    HttpEntity resEntity;

    String ImageNameFieldOnServer = "image_name" ;

    String GetImageNameFromEditText;

    String ImagePathFieldOnServer = "image_path" ;


    String ImagePathFieldOnServer2 = "image_path2" ;



    String ImageUploadPathOnSever ="http://mastekcsp.hostingerapp.com/Mastek/Basic/focal.php" ;

    String city,state, area, postalcode, data1,data2;

    DbHelper dbHelper;

    EditText imgdesc;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploadreport);

        img1 = (ImageView)findViewById(R.id.imageView);
        img2 = (ImageView)findViewById(R.id.imageView2);
        txt1 = (TextView)findViewById(R.id.baseline);
        txt2 = (TextView)findViewById(R.id.focallength);
        txt3 = (TextView)findViewById(R.id.ccdwidth);
        txt4 = (TextView)findViewById(R.id.locationText);

        imgdesc = (EditText)findViewById(R.id.imgdesc);

        b3  = (Button) findViewById(R.id.button2);
        imgclear = (Button) findViewById(R.id.imgclear);

        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Custcamera.bitmap = Bitmap.createBitmap(Custcamera.bitmap, 0, 0, Custcamera.bitmap.getWidth(), Custcamera.bitmap.getHeight(), matrix, true);
        Custcamera.bitmap2 = Bitmap.createBitmap(Custcamera.bitmap2, 0, 0, Custcamera.bitmap2.getWidth(), Custcamera.bitmap2.getHeight(), matrix, true);


        img1.setImageBitmap(Custcamera.bitmap);
        img2.setImageBitmap(Custcamera.bitmap2);

        //txt1.setText("Baseline is "+Custcamera.baseline);





        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String[] cameraIds = manager.getCameraIdList();
            if (cameraIds.length > 1) {
                    CameraCharacteristics character = manager.getCameraCharacteristics(cameraIds[1]);
                    size = character.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
            }
        }
        catch (CameraAccessException e)
        {
            Log.e("YourLogString", e.getMessage(), e);
        }

        txt3.setText("New CCDWidth "+size);



        mCamera=Camera.open();
        Camera.Parameters params = mCamera.getParameters();
        focalLength =  params.getFocalLength();
         horizontalViewAngle = params.getHorizontalViewAngle();
         verticalViewAngle = params.getVerticalViewAngle();

         width = (float) (tan(horizontalViewAngle/2) *2 * focalLength);

        txt2.setText("focal length" + focalLength);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

            getLocation();


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               GetImageNameFromEditText = imgdesc.getText().toString();
                ImageUploadToServerFunction();

            }
        });

        imgclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Custcamera.bitmap = null;
                Custcamera.bitmap2 = null;
                Custcamera.baseline = 0;
                Intent in = new Intent(Uploadreport.this , Custcamera.class);
                startActivity(in);
                finish();
            }
        });


       /* Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Custcamera.bitmap =  Bitmap.createBitmap(Custcamera.bitmap, 0, 0, Custcamera.bitmap.getWidth(), Custcamera.bitmap.getHeight(), matrix, true);
        Custcamera.bitmap2 =  Bitmap.createBitmap(Custcamera.bitmap2, 0, 0, Custcamera.bitmap2.getWidth(), Custcamera.bitmap2.getHeight(), matrix, true);*/

       dbHelper = new DbHelper(this);
    }

    @Override
    public void onBackPressed() {

        Custcamera.bitmap = null;
        Custcamera.bitmap2 = null;
        Custcamera.baseline=0.00;

        Intent i = new Intent(getApplicationContext(), Dashboard.class);
        startActivity(i);
        finish();
    }


    public void addToDb(View view){

         /*data1 = savetoMem(Custcamera.bitmap);
         data2 = savetoMem(Custcamera.bitmap2);
        addtoDB();*/

        //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

        img1.setDrawingCacheEnabled(true);
        img1.buildDrawingCache();
        Bitmap bitmapl1 = img1.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapl1.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data1 = baos.toByteArray();

        img1.setDrawingCacheEnabled(true);
        img1.buildDrawingCache();
        Bitmap bitmapl2 = img1.getDrawingCache();
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        bitmapl2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
        byte[] data2 = baos2.toByteArray();

        GetImageNameFromEditText = imgdesc.getText().toString();
        dbHelper.addToDb(data1, data2, String.valueOf(lati), String.valueOf(longi), area, city, postalcode, String.valueOf(focalLength),
                String.valueOf(size), String.valueOf(Custcamera.baseline), String.valueOf(GetImageNameFromEditText));
        Toast.makeText(this, "Image saved to DB successfully", Toast.LENGTH_SHORT).show();

    }


    public void addtoDB(){

      /*  dbHelper.addToDb(data1, data2, String.valueOf(lati), String.valueOf(longi), area, city, postalcode, String.valueOf(focalLength),
                String.valueOf(size), String.valueOf(Custcamera.baseline));
        Toast.makeText(this, "Image saved to DB successfully", Toast.LENGTH_SHORT).show();*/

    }


    public String savetoMem(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.

        if (!wallpaperDirectory.exists()) {
            Log.d("dirrrrrr", "" + wallpaperDirectory.mkdirs());
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";

    }




    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        lati = location.getLatitude();
        longi = location.getLongitude();

        txt4.setText("Latitude: " + lati + "\nLongitude: " + longi);

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            txt4.setText(txt4.getText() + area + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));

            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();

             postalcode = addresses.get(0).getPostalCode();
            area = addresses.get(0).getSubLocality();




        }catch(Exception e) {

        }





    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(Uploadreport.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }



    public void ImageUploadToServerFunction(){


        ByteArrayOutputStream byteArrayOutputStreamObject,byteArrayOutputStreamObject2 ;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        byteArrayOutputStreamObject2 = new ByteArrayOutputStream();

        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
        Custcamera.bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamObject);
        Custcamera.bitmap2.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamObject2);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        byte[] byteArrayVar2 = byteArrayOutputStreamObject2.toByteArray();




        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        final String ConvertImage2 = Base64.encodeToString(byteArrayVar2, Base64.DEFAULT);

       // CaptureImageFromCamera.setVisibility(View.VISIBLE);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                // Showing progress dialog at image upload time.
                progressDialog = ProgressDialog.show(Uploadreport.this,"Image is Uploading","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                Toast.makeText(Uploadreport.this,string1,Toast.LENGTH_LONG).show();

                // Setting image as transparent after done uploading.
                //ImageViewHolder.setImageResource(android.R.color.transparent);

                if(string1.equalsIgnoreCase("Your Image Has Been Uploaded.")){

                    Custcamera.bitmap = null;
                    Custcamera.bitmap2 = null;
                    Custcamera.baseline=0;

                    Intent in = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(in);
                }


            }

            @Override
            protected String doInBackground(Void... params) {

                Uploadreport.ImageProcessClass imageProcessClass = new Uploadreport.ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                //HashMapParams.put(ImageNameFieldOnServer, GetImageNameFromEditText);

                HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                HashMapParams.put(ImagePathFieldOnServer2, ConvertImage2);

                HashMapParams.put("lati", String.valueOf(lati));

                HashMapParams.put("longi", String.valueOf(longi));

                HashMapParams.put("baseline", String.valueOf(Custcamera.baseline));

                HashMapParams.put("focal_length", String.valueOf(focalLength));

                HashMapParams.put("ccdwidth", String.valueOf(size));

                HashMapParams.put("Emp_email", MainActivity.EmailHolder);

                HashMapParams.put("city", city);

                HashMapParams.put("street", area);

                HashMapParams.put("pincode", postalcode);

                HashMapParams.put("imgdes", GetImageNameFromEditText);

                //HashMapParams.put("empid", String.valueOf(width));

                // HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                String FinalData = imageProcessClass.ImageHttpRequest(ImageUploadPathOnSever, HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);
                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);
                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(new OutputStreamWriter(OutPutStream, "UTF-8"));
                bufferedWriterObject.write(bufferedWriterDataFN(PData));
                bufferedWriterObject.flush();
                bufferedWriterObject.close();
                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){
                        stringBuilder.append(RC2);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;
            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilderObject.append("&");
                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilderObject.append("=");
                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }


}
