package com.example.csp_6;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Blob;

import static android.provider.BaseColumns._ID;

/**
 * Created by delaroy on 9/8/17.
 */

public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = DbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "image.db";
    private static final int DATABASE_VERSION = 1;
    Context context;
    SQLiteDatabase db;
    ContentResolver mContentResolver;

    public final static String COLUMN_NAME = "imagename1";
    public final static String imagename2 = "imagename2";
    public final static String Latil = "Lati";
    public final static String Logil = "Logi";
    public final static String areal = "area";
    public final static String cityl = "city";
    public final static String pincodel = "pincode";
    public final static String focall = "focallength";
    public final static String ccdwidth1 = "ccdwidth";
    public final static String baseline1 = "baseline";
    public final static String checkl = "checkit";
    public final static String useremaill= "useremail";
    public final static String Description= "desc";



    public final static String TABLE_NAME = "imagetable";




    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContentResolver = context.getContentResolver();

        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_IMAGE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME + " BLOB NOT NULL,"+imagename2+" BLOB NOT NULL," + Latil + " VARCHAR," +
                Logil + " VARCHAR," + areal + " VARCHAR," + cityl+ " VARCHAR," + pincodel+ " VARCHAR," +
                ccdwidth1+ " VARCHAR," +focall+ " VARCHAR," + baseline1+ " VARCHAR," + checkl+ " INT," +
                useremaill+ " VARCHAR, " + Description + "VARCHAR"
                + " );";

        db.execSQL(SQL_CREATE_IMAGE_TABLE);

        Log.d(TAG, "Database Created Successfully" );

    }

    public void addToDb(byte[] image, byte[] image2, String latip, String longip, String areap,
                        String cityp, String pincodep, String focalp, String ccdwidthp, String baselinep, String description){
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME,   image);
        cv.put(imagename2,   image2);
        cv.put(Latil, latip);
        cv.put(Logil, longip);
        cv.put(areal, areap);
        cv.put(cityl,   cityp);
        cv.put(pincodel,   pincodep);
        cv.put(focall,focalp);
        cv.put(ccdwidth1,   ccdwidthp);
        cv.put(baseline1,   baselinep);
        cv.put(useremaill,   MainActivity.EmailHolder);
        cv.put(Description, description);

        db.insert( TABLE_NAME, null, cv );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Cursor getData(){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT * FROM "+TABLE_NAME;
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public Cursor deleteData(){

        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT * FROM "+TABLE_NAME;
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public Cursor upData(int repid){

        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT * FROM "+TABLE_NAME+" WHERE _ID = "+repid;
        Cursor data = db.rawQuery(query,null);
        return data;
    }

}

