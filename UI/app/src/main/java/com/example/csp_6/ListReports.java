package com.example.csp_6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.csp_6.instances.DataAdapter;
import com.example.csp_6.instances.DataReps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListReports extends AppCompatActivity {
    JSONArray jsonArray;
    JSONObject jsonObject;
    DataAdapter dataAdapter;
    ListView mListView;
    String data;
    int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_reports);



        dataAdapter = new DataAdapter(this, R.layout.view_adapter);
        mListView=(ListView)findViewById(R.id.reports);
        mListView.setAdapter(dataAdapter);

        String jsonstring = getIntent().getExtras().getString("json_data");
    try {
        jsonObject = new JSONObject(jsonstring);
        jsonArray = jsonObject.getJSONArray("response");
        Log.e("res",String.valueOf(jsonArray));

        while (count<jsonArray.length()){
            JSONObject jo = jsonArray.getJSONObject(count);
            DataReps rep = new DataReps();
            rep.setlikec(true);
            rep.setReptitle(jo.getString("rep_id"));
            rep.setCity_name(jo.getString("street"));
            rep.setImage_url(jo.getString("image_1"));
            rep.setStatus(jo.getString("status"));
            rep.setDescp(jo.getString("Description"));
            dataAdapter.add(rep);
            count++;
        }
    } catch (
    JSONException e) {
        e.printStackTrace();
    }




    }
}
