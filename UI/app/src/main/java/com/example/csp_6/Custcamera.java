package com.example.csp_6;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static java.sql.Types.NULL;

public class Custcamera extends AppCompatActivity {


    public static double baseline=0;
    TextView tv;


    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private ImageButton capture, switchCamera;
    private Context myContext;
    private LinearLayout cameraPreview;
    private boolean cameraFront = false;
    public static Bitmap bitmap; //first image
    public static Bitmap bitmap2;

    public static int count=1;

    ImageButton reset;

    private ImageView imgview;

    Camera.Parameters params;

    List<Camera.Size> sizes;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custcamera);

        tv = (TextView) findViewById(R.id.txt);

        imgview = findViewById(R.id.imgpreview1);
        imgview.setRotation(90);
        imgview.setImageBitmap(Custcamera.bitmap);
        imgview.getLayoutParams().height = 200;
        imgview.getLayoutParams().width = 200;


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;

        mCamera =  Camera.open();
        mCamera.setDisplayOrientation(90);
        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);
        params = mCamera.getParameters();

        reset = (ImageButton) findViewById(R.id.restart);

        reset.setVisibility(View.INVISIBLE);


        mCamera = Camera.open(0);
        mCamera.setDisplayOrientation(90);

        sizes = params.getSupportedPictureSizes();
        Camera.Size size = sizes.get(0);
//Camera.Size size1 = sizes.get(0);
        for(int i=0;i<sizes.size();i++)
        {

            if(sizes.get(i).width > size.width)
                size = sizes.get(i);


        }


       // Camera.Parameters params = camera.getParameters();
         sizes = params.getSupportedPictureSizes();


        Camera.Size sizePicture = sizes.get(1);
        params.setPictureSize(sizePicture.width, sizePicture.height);


        // params.setPictureSize(size.width, size.height);
        params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
        params.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
        params.setExposureCompensation(0);
        params.setPictureFormat(ImageFormat.JPEG);
        //params.setRotation(90);
        params.getSupportedPictureSizes();
        params.setJpegQuality(100);
        params.setRotation(90);



        mCamera.setParameters(params);



        //mCamera.startPreview();


        mPicture = getPictureCallback();
        mPreview.refreshCamera(mCamera);

        capture = (ImageButton) findViewById(R.id.btnCam);


        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCamera.takePicture(null, null, mPicture);
            }
        });



        switchCamera = (ImageButton) findViewById(R.id.btnSwitch);
        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the number of cameras
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    //release the old camera instance
                    //switch camera, from the front and the back and vice versa

                    releaseCamera();
                    chooseCamera();
                } else {

                }
            }
        });


       /* parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        List<Camera.Size> sizes=parameters.getSupportedPictureSizes();
        parameters.setPictureSize(sizes.get(0).width, sizes.get(0).height);
        mCamera.setParameters(parameters);

        parameters.getSupportedPreviewSizes();
       // parameters.setPreviewSize(400,400);*/

    //baseline
        if(bitmap !=null){
            initialiseAccelerometer();
            startBaselineMeasurement();

            reset.setVisibility(View.VISIBLE);
            tv.setText("Move your device slightly toward right");
        }
        //end of baseline

    }
        //baseline
        public void restart(View view){

            xyz.initialise();

        }
        //end


    @Override
    protected void onResume() {
        super.onResume();


        if (mCamera == null) {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            Log.d("nu", "null");
        } else {
            Log.d("nu", "no null");
        }


    }
    @Override
    protected void onPause() {

        super.onPause();


        releaseCamera();
    }



    //baseline code





    void initialiseAccelerometer()
    {
       // tv.setText("Initialising accelerometer. Please wait...");
        SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Accelerometer.initialiseAccelerometer(mSensorManager);
        xyz = Accelerometer.sensor;
       // tv.setText("Accelerometer inialised...");



    }

    public void btRestart(View v)
    {
        xyz.initialise();

    }


    void startBaselineMeasurement()
    {

        hBaseline = new Handler(){

            @Override
            public void handleMessage(Message msg)
            {
                switch (msg.what) {
                    case 0:
                    {
                        timer = new Timer();
                        timer.scheduleAtFixedRate(
                                new TimerTask() {

                                    public void run() {

                                        xyz.ProcessData();

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(xyz.waitTillAccelerometerIsSteady) {
                                                  //  tv.setText("Hold phone steady parallel to the floor for accurate measurements");
                                                   // tv.append(String.format("\naX aY = %.3fm,%.3fm", xyz.AccelX, xyz.AccelY));
                                                }
                                                else {
                                                    /*if(xyz.getBaselineInMM() < 50)
                                                        tv.setText("Move phone horizontally parallel to pothole");
                                                    else
                                                        tv.setText("Now please take second picture");*/
                                                  //  tv.append(String.format("\naX aY = %.3fm,%.3fm", xyz.AccelX, xyz.AccelY));
                                                   // tv.append(String.format("\nX Y = %.3fm,%.3fm baseline = %.3fmm %.3fsec", xyz.distXR, xyz.distYR,  xyz.getBaselineInMM(), xyz.interval));
                                                }
                                            }
                                        });


                                    }
                                },
                                0,
                                UPDATE_INTERVAL);

                        xyz.initialise();

                        break;

                    }
                    case 1:
                    {
                        timer.cancel();
                        timer = null;

                    }
                }


            }


        };
        hBaseline.sendEmptyMessage(0);

    }



    void stopBaselineMeasurement()
    {
        hBaseline.sendEmptyMessage(1);
        hBaseline = null;
    }
    Accelerometer xyz;
    private static final long UPDATE_INTERVAL = 100;
    private Timer timer;

    Handler hBaseline;


    @Override
    protected void onDestroy() {

        Accelerometer.unRegister((SensorManager) getSystemService(Context.SENSOR_SERVICE));
        super.onDestroy();
    }

    //end of baseline code




    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }


    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }


    public void chooseCamera() {

        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
    }


    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }




    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                if(count ==1) {
                    count++;
                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.length); // changed when 1st image is clicked
                    Intent intent = new Intent(Custcamera.this, Custcamera.class);  //when 1st image is clicked then...
                    startActivity(intent); //So when this funcion is called onResume event will be fired correct? right
                }
                else {

                    bitmap2 = BitmapFactory.decodeByteArray(data, 0, data.length);
                    count = 1;
                    Intent intent = new Intent(Custcamera.this, Uploadreport.class);
                    startActivity(intent); //ok so now the problem is moment you hold the phone the baseline will stop measuring and initialise to zero... yes but i will be sending its lastest value to the variable where is that code?
                }
            }
        };
        return picture;
    }




}
