package com.example.csp_6.instances;

import android.graphics.Bitmap;

public class Images {

    public static int cap_count = 0;

    float focal_length, ccd_width;

    Bitmap image_string_one;
    Bitmap image_string_two;

    public float getFocal_length() {
        return focal_length;
    }

    public void setFocal_length(float focal_length) {
        this.focal_length = focal_length;
    }


    public float getCcd_width() {
        return ccd_width;
    }

    public void setCcd_width(float ccd_width) {
        this.ccd_width = ccd_width;
    }

    public Bitmap getImage_string_one() {
        return image_string_one;
    }

    public void setImage_string_one(Bitmap image_string_one) {
        this.image_string_one = image_string_one;
    }

    public Bitmap getImage_string_two() {
        return image_string_two;
    }

    public void setImage_string_two(Bitmap image_string_two) {
        this.image_string_two = image_string_two;
    }
}
