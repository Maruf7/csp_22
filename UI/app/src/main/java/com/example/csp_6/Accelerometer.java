package com.example.csp_6;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;

//ref: https://stackoverflow.com/questions/30443727/integrating-android-accelerometer-to-find-velocity-and-position
public class Accelerometer implements SensorEventListener
{

    public double AccelX, AccelY, AccelZ;
    public double accelX, accelY, accelZ;
    double prevAccelX, prevAccelY, prevAccelZ;
    double prevTime;
    public double velX, velY, velZ;
    double prevVelX, prevVelY, prevVelZ;
    public double distX, distY, distZ;
    double sprevAccelX, sprevAccelY, sprevAccelZ;

    ArrayList<Double  []> storedValues;
    public Accelerometer()
    {

        distX = distY = distZ = 0;
        storedValues= new ArrayList<Double []>();
    }

    public void initialise()
    {
        prevAccelX = prevAccelY = prevAccelZ = 0;
        sprevAccelX = sprevAccelY = sprevAccelZ = 0;
        prevVelX = prevVelY = prevVelZ = 0;
        distX = distY = distZ = 0;
        prevTime = 0;
        waitTillAccelerometerIsSteady = true;
        prevVelX = prevVelY = prevVelZ = 0;
        distXR = distYR = distZR = 0;
        storedValues.clear();
    }
    double accelKFactor = 0.01;

    public boolean waitTillAccelerometerIsSteady = true;
    public double interval;
    double currentTime;
    @Override
    public void onSensorChanged(SensorEvent event) {
        AccelX = event.values[0];
        AccelY = event.values[1];
        AccelZ = event.values[2];
        currentTime = System.currentTimeMillis() / 1000.0;// event.timestamp /1000000000.0;

        if(Math.abs(AccelX) > 1.0 || Math.abs(AccelY)> 1.0) {
            storedValues.clear();

            sprevAccelX = accelX;
            sprevAccelY = accelY;
            sprevAccelZ = accelZ;
           /* distX += distXR;
            distY += distYR;
            distZ += distZR;*/
            distXR = distYR = distZR = 0;
            return;
        }
        if(Math.abs(AccelX - prevAccelX) > 0.012 || Math.abs(AccelY - prevAccelY) > 0.012 ) {
            Double[] data = new Double[]{AccelX, AccelY, AccelZ, currentTime};
            storedValues.add(data);


            prevAccelX = AccelX;
            prevAccelY = AccelY;
            prevAccelZ = AccelZ;
        }
        else
        {
            storedValues.clear();

            sprevAccelX = accelX;
            sprevAccelY = accelY;
            sprevAccelZ = accelZ;

            distX += distXR;
            distY += distYR;
            distZ += distZR;
            distXR = distYR = distZR = 0;

        }

    }

    public void ProcessData()
    {

        if(storedValues.size() <= 1)
            return;

        try {
            Double[] avg = new Double[]{0.0, 0.0, 0.0, 0.0};
            double minTime = Double.MAX_VALUE;
            double maxTime = 0;

            for (int i = 0; i < storedValues.size() - 1; i++) {
                for (int j = 0; j < avg.length - 1; j++)
                    avg[j] += storedValues.get(i)[j];

                if (minTime > storedValues.get(i)[3])
                    minTime = storedValues.get(i)[3];

                if (maxTime < storedValues.get(i)[3])
                    maxTime = storedValues.get(i)[3];
            }

            for (int j = 0; j < avg.length - 1; j++)
                avg[j] /= storedValues.size();

            interval = maxTime - minTime;

            accelX = avg[0];
            accelY = avg[1];
            accelZ = avg[2];

            if(waitTillAccelerometerIsSteady)
            {
                double threshold = 1.0;
                if(Math.abs(accelX) < threshold && Math.abs(accelY) < threshold) {
                    waitTillAccelerometerIsSteady = false;
                    sprevAccelX = accelX;
                    sprevAccelY = accelY;
                    sprevAccelZ = accelZ;
                    storedValues.clear();
                    distX += distXR;
                    distY += distYR;
                    distZ += distZR;
                    distXR = distYR = distZR = 0;
                    return;
                }
            }

            accelX -= sprevAccelX;
            accelY -= sprevAccelY;
            accelZ -= sprevAccelZ;


            velX = accelX * interval;
            velY = accelY * interval;
            velZ = accelZ * interval;

            distXR = velX * interval;
            distYR = velY * interval;
            distZR = velZ * interval;
        }
        catch (Exception e)
        {

        }

    }

    public double distXR;
    public double distYR;
    public double distZR;

    public void ProcessData1()
    {
/*
        if(sprevAccelX == 0) {
            sprevAccelX = accelX;
            sprevAccelY = accelY;
            sprevAccelZ = accelZ;
            return;
        }*/

        accelX = AccelX - sprevAccelX;
        accelY = AccelY - sprevAccelY;
        accelZ = AccelZ - sprevAccelZ;


/*
        accelX = (accelX * accelKFactor) + prevAccelX * (1 - accelKFactor);
        accelY = (accelY * accelKFactor) + prevAccelY * (1 - accelKFactor);
        accelZ = (accelZ * accelKFactor) + prevAccelZ * (1 - accelKFactor);

*/
        if(waitTillAccelerometerIsSteady)
        {
            double threshold = 0.01;
            if(Math.abs(accelX) < threshold && Math.abs(accelY) < threshold)
                waitTillAccelerometerIsSteady = false;
            else
            {
                sprevAccelX = accelX;
                sprevAccelY = accelY;
                sprevAccelZ = accelZ;
            }

            return;
        }




        // currentTime = System.currentTimeMillis() / 1000.0;

        if(prevTime == 0) prevTime = currentTime;

        interval = currentTime - prevTime;

        //if(interval < 0.5)
        //  return;

        prevTime = currentTime;

        if(Math.abs(accelX) < 0.005 && Math.abs(accelY) < 0.005 )
            return;

        if(false) {
            velX += accelX * interval;
            velY += accelY * interval;
            velZ += accelZ * interval;
        }
        else {
            velX = accelX * interval;
            velY = accelY * interval;
            velZ = accelZ * interval;
        }
        distX += prevVelX + velX * interval;
        distY += prevVelY + velY * interval;
        distZ += prevVelZ + velZ * interval;

        prevAccelX = accelX;
        prevAccelY = accelY;
        prevAccelZ = accelZ;

        /*
        prevVelX = velX;
        prevVelY = velY;
        prevVelZ = velZ;
        */
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public double getBaselineInMM()
    {
        return Math.max(Math.abs(distX), Math.abs(distY))*1000.0;
    }

    public static Accelerometer sensor = null;
    public static Sensor Hardware;

    public static  void initialiseAccelerometer(SensorManager mSensorManager)
    {
        if(sensor == null) {
            sensor = new Accelerometer();
            Hardware = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            mSensorManager.registerListener(sensor, Hardware
                    ,
                    SensorManager.SENSOR_DELAY_NORMAL);   //again crashed
        }
    }

    public static void unRegister(SensorManager mSensorManager)
    {
        if(sensor!=null)
            mSensorManager.unregisterListener(sensor);


    }

}