package com.example.csp_6.Server;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Log;

import com.example.csp_6.ListReports;
import com.example.csp_6.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FetchMyData extends AsyncTask<String, Void, String>{
    Context ctx;
     String root_url = "http://mastekcsp.hostingerapp.com/Mastek/Basic/reponse_reports.php?email="+MainActivity.EmailHolder;


    HttpURLConnection httpsURLConnection;

    public FetchMyData(Context ctx){
        this.ctx = ctx;
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            URL url = new URL(root_url);
            httpsURLConnection = (HttpURLConnection) url.openConnection();
            Log.e("I M here",MainActivity.EmailHolder);

        }catch(Exception e){
            Log.d("Conenction error",e.toString());
        }
        InputStream inputStream = null;
        try {
            inputStream = httpsURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringbuild = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringbuild.append(line);
            }

            return stringbuild.toString().trim();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    @Override
    protected void onPreExecute() {


        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String json_string) {
        Intent i = new Intent(ctx, ListReports.class);
        i.putExtra("json_data", json_string);
        ctx.startActivity(i);
    }

}
