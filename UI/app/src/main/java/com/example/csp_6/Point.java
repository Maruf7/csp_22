package com.example.csp_6;

public class Point {
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private int cnt = 1;

    public double getX() {
        return x/(float)cnt;
    }

    public double getY() {
        return y/(float)cnt;
    }

    public double getZ() {
        return z/(float)cnt;
    }

    public Point(double x, double y, double z, int cnt) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.cnt = cnt;
    }


    public double getForce(){
        return getX()*getX()+getY()*getY()+getZ()*getZ();
    }
}
