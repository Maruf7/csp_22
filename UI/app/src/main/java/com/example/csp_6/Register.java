package com.example.csp_6;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Register extends Activity{

    //   TextView tvIsConnected;
    EditText etName, etEmail, etPhone, etAddress, etuname, etpass, etpassconf;
    Button btnPost;
    String TempName, TempEmail, TempAdd, TempUser, TempPass, TempNum, TempPass2;
    String ServerURL = "http://mastekcsp.hostingerapp.com/Mastek/Basic/insertEmp.php" ;
    String res1;
    HttpResponse httpResponse;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // get reference to the views
        //   tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
        etName = (EditText) findViewById(R.id.so_name);
        etEmail = (EditText) findViewById(R.id.so_email);
        etPhone = (EditText) findViewById(R.id.so_phone);
        /*etAddress = (EditText) findViewById(R.id.so_address);
        etuname = (EditText) findViewById(R.id.so_user);*/
        etpass = (EditText) findViewById(R.id.so_pass);
        etpassconf = (EditText) findViewById(R.id.so_pass_confirm);
        btnPost = (Button) findViewById(R.id.btnPost);


        // add click listener to Button "POST"
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetData();
                if(TempPass.equals(TempPass2)) {
                    InsertData(TempName, TempEmail, TempNum , TempPass);

                }
                else
                    Toast.makeText(getApplicationContext(),"Passwords do not match. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void GetData() {

        TempName = etName.getText().toString();
        TempEmail = etEmail.getText().toString();
        TempNum = etPhone.getText().toString();
        /*TempAdd = etAddress.getText().toString();
        TempUser = etuname.getText().toString();*/
        TempPass = etpass.getText().toString();
        TempPass2 = etpassconf.getText().toString();

    }

    public void InsertData(final String name, final String email, final String number, final String password){

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {


            @Override
            protected void onPostExecute(String result) {

                super.onPostExecute(result);

                Toast.makeText(Register.this, result, Toast.LENGTH_LONG).show();

            }



            @Override
            protected String doInBackground(String... params) {

                String NameHolder = name;
                String EmailHolder = email;
                String PhoneHolder = number;
                /*String AddHolder = addres;
                String UserHolder = user;*/
                String PassHolder = password;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("so_name", NameHolder));
                nameValuePairs.add(new BasicNameValuePair("so_email", EmailHolder));
                nameValuePairs.add(new BasicNameValuePair("so_phone", PhoneHolder));
                /*nameValuePairs.add(new BasicNameValuePair("so_address", AddHolder));
                nameValuePairs.add(new BasicNameValuePair("so_user", UserHolder));*/
                nameValuePairs.add(new BasicNameValuePair("so_pass", PassHolder));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(ServerURL);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    httpResponse = httpClient.execute(httpPost);

                    HttpEntity httpEntity = httpResponse.getEntity();
                    //res1 = httpEntity.toString();

                } catch (ClientProtocolException e) {

                    res1 = "error1";

                } catch (IOException e) {
                    res1 = "error2";
                }


                try{
                    BufferedReader rd = new BufferedReader(new InputStreamReader(
                            httpResponse.getEntity().getContent()));
                    String line="";
                    res1 = "";
                    while((line = rd.readLine()) != null){
                        res1 = res1 + line;
                    }


                } catch(Exception e){
                    res1 = "error3";
                }



                return res1;
            }


        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();

        sendPostReqAsyncTask.execute(name, email);
    }

}

