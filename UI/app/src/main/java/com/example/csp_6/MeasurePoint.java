package com.example.csp_6;

public class MeasurePoint {
    public double x;
    public double y;
    public double z;
    public double speedBefore;
    public double speedAfter;
    public double distance;
    public double acceleration;
    public long interval;
    public Point averagePoint;

    public MeasurePoint(double x, double y, double z, double speedBefore, long interval, Point averagePoint) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.speedBefore = speedBefore;
        this.interval = interval;
        this.averagePoint = averagePoint;
        speedAfter = 0;
        calc();
    }

    private void calc(){
        //Acceleration as projection of current vector on average
        acceleration = this.x*averagePoint.getX() +
                this.y*averagePoint.getY() +
                this.z*averagePoint.getZ();
        acceleration = acceleration / ((float)Math.sqrt(averagePoint.getForce()));
        float t = ((float)interval / 1000f);
        speedAfter = speedBefore + acceleration * t;
        distance = speedBefore*t + acceleration*t*t/2;

    }

    public String getStoreString(){
        String s = "write here whatever you want";
        return s;
    }

// add getters
}

