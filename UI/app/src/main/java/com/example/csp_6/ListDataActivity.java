package com.example.csp_6;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.csp_6.instances.DataAdapter;
import com.example.csp_6.instances.DataReps;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ListDataActivity extends AppCompatActivity {
    DataAdapter dataAdapter;
     ListView mListView;

    private static final String TAG="ListDataActivity";
    DbHelper mDatabseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        dataAdapter = new DataAdapter(this, R.layout.view_adapter);
        mListView=(ListView)findViewById(R.id.listView);
        mListView.setAdapter(dataAdapter);
        mDatabseHelper = new DbHelper(this);


        try {
            populateListView();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public void  populateListView() throws UnsupportedEncodingException {
        Log.d(TAG,"PopulateListView:Dispalying DAta in List View");
        Cursor data= mDatabseHelper.getData();
        ArrayList<String> listData = new ArrayList<>();
        while (data.moveToNext()) {

            //String words = new String(data.getBlob(1), "UTF-8");

            DataReps dataReps = new DataReps();
            dataReps.setReptitle(data.getString(0));

            dataReps.setDescp(data.getString(6));
            dataReps.setCity_name(data.getString(5));
            dataReps.setimgBlob(data.getBlob(1));
            dataReps.setcheckBlob(true);
            dataReps.setuploadc(true);
//            dataReps.setStatus(data.getString(4));
            dataAdapter.add(dataReps);
        }
    }
  private void toastMessage(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

