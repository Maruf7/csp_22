package com.example.csp_6;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.csp_6.Server.FetchData;
import com.example.csp_6.Server.FetchMyData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Dashboard extends AppCompatActivity {
    ImageButton b1,b2,b3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        b1=(ImageButton)findViewById(R.id.button2);
        b2=(ImageButton)findViewById(R.id.button5);
        b3 = (ImageButton)findViewById(R.id.button3);
    }
    public void addreport(View view) {
        Intent intent1 = new Intent(getApplicationContext(), CLocation2.class);
        startActivity(intent1);
    }
    public void signout(View view)
    {
        Intent intent2=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent2);
        finish();
    }

    public void viewreport(View view)
    {
       /* */
        FetchData fetchData = new FetchData(view.getContext());
        fetchData.execute();
    }

    public void viewsavedreports(View view)
    {
        FetchMyData fetchData = new FetchMyData(view.getContext());
        fetchData.execute();
    }

    public void mySavedreports(View view){

        Intent im = new Intent(getApplicationContext(),ListDataActivity.class);
        startActivity(im);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}

/*class GetMyReports extends AsyncTask<String, Void, String> {

    private Context ctx;
    private String fetchurl = "https://mastekcsp.000webhostapp.com/Mastek/Basic/reponse_reports.php?" + MainActivity.EmailHolder;
    private ProgressDialog progressDialog;
    private int turn;
    GetMyReports(Context ctx, int turn){
        this.ctx = ctx;
        this.turn = turn;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {

        if(progressDialog.isShowing()){
            progressDialog.cancel();
        }
        switch (turn){
            case 1:
                Intent i = new Intent(ctx,Myreports.class);
                i.putExtra("jsonstring",s);
                ctx.startActivity(i);
                break;
            *//*case 2:
                Intent i2 = new Intent(ctx,Trending.class);
                i2.putExtra("jsonstring",s);
                ctx.startActivity(i2);
                break;*//*
        }


    }

    @Override
    protected String doInBackground(String... strings) {
        //Token token = new Token(ctx,null,null,1);
        fetchurl = "https://mastekcsp.000webhostapp.com/Mastek/Basic/reponse_reports.php?email='" + MainActivity.EmailHolder+"'";
        if(turn==2)
          //  fetchurl = User.rooturl+"reportsjson.php?trending=true";

        try {
            URL url = new URL(fetchurl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine()) != null){

                stringBuilder.append(line);
            }

            return stringBuilder.toString().trim();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}*/
