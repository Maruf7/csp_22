package com.example.csp_6.instances;

import java.sql.Blob;

public class DataReps {
    private String reptitle;
    private String image_url;
    private String city_name;
    private String status;
    private String descp;
    private boolean cam = false;
    private byte[] imgBlob;
    private boolean like = false;
    private boolean upload= false;



    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getReptitle(){return reptitle;}

    public void setReptitle(String reptitle){this.reptitle = reptitle;}


    public Boolean getcheckBlob(){return cam;}

    public void setcheckBlob(Boolean cam){this.cam = cam;}

    public Boolean getuploadc(){return upload;}

    public void setuploadc(Boolean cam){this.upload = upload;}

    public Boolean getlikec(){return like;}

    public void setlikec(Boolean cam){this.like = like;}


    public byte[] getimgBlob(){return imgBlob;}

    public void setimgBlob(byte[] imgBlob){this.imgBlob = imgBlob;}





}
