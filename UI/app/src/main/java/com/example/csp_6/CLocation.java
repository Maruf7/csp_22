package com.example.csp_6;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.csp_6.instances.Images;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Policy;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

import static java.lang.Math.tan;


public class CLocation extends AppCompatActivity implements LocationListener {


    /*for baseline*/

    TextView tv;
    Button testBtn;
    static final int TIMER_DONE = 2;
    static final int START = 3;
    static final int CAL_TIMER_DONE = 4;
    static final int ERROR = 5;





    Images images;

    Button CaptureImageFromCamera,UploadImageToServer,save,view;

    DatabaseHelper mDatabaseHelper;

    ImageView ImageViewHolder, ImageViewHolder2;

    EditText editText;

    TextView locationText,focal;

    Intent intent1, intent2;

    Bitmap bitmap;

    LocationManager locationManager;

    public  static final int RequestPermissionCode  = 1 ;

    boolean check = true;

    ProgressDialog progressDialog;

    String GetImageNameFromEditText;

    String ImageNameFieldOnServer = "image_name" ;

    String ImagePathFieldOnServer = "image_path" ;

    String ImageUploadPathOnSever ="https://mastekcsp.000webhostapp.com/Mastek/Basic/insertLatiLongi.php" ;

    double lati;
    double longi;
    EditText imageName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clocation);
        mDatabaseHelper =new DatabaseHelper(this);

        images = new Images();
        editText=(EditText)findViewById(R.id.editText);
        CaptureImageFromCamera = (Button)findViewById(R.id.button);
        ImageViewHolder = (ImageView)findViewById(R.id.imageView);
        ImageViewHolder2 = (ImageView)findViewById(R.id.imageView2);
        ImageViewHolder2.setVisibility(View.INVISIBLE);
        ImageViewHolder2.getLayoutParams().height = 0;
        UploadImageToServer = (Button) findViewById(R.id.button2);
        save=(Button)findViewById(R.id.button3);
        view=(Button)findViewById(R.id.button4);

        tv = (TextView) findViewById(R.id.txt);

        locationText = (TextView)findViewById(R.id.locationText);
        focal = (TextView)findViewById(R.id.focal);
        imageName = (EditText) findViewById(R.id.editText);

        Camera mCamera;
        mCamera=Camera.open();
        Camera.Parameters params = mCamera.getParameters();
        float focalLength =  params.getFocalLength();
        int horizontalViewAngle = (int) params.getHorizontalViewAngle();
        float verticalViewAngle = params.getVerticalViewAngle();

        float width = (float) (tan(horizontalViewAngle/2) *2 * focalLength);
        images.setFocal_length(width);
        images.setCcd_width(width);
        focal.setText("focal length" + focalLength +" CCDwidth "+width);



        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        EnableRuntimePermissionToAccessCamera();

        CaptureImageFromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent1 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                startActivityForResult(intent1,7);

                getLocation();

                /*for baseline*/





            }
        });

        UploadImageToServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetImageNameFromEditText = imageName.getText().toString();
                ImageUploadToServerFunction();

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newEntry = editText.getText().toString();
                if(editText.length()!=0)
                {
                    AddData(newEntry);
                }
                else
                {
                    toastMessage("Put something in text");
                }
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent2 = new Intent(getApplication(),ListDataActivity.class);
                startActivity(intent2);
            }
        });


        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Custcamera.bitmap =  Bitmap.createBitmap(Custcamera.bitmap, 0, 0, Custcamera.bitmap.getWidth(), Custcamera.bitmap.getHeight(), matrix, true);
        Custcamera.bitmap2 =  Bitmap.createBitmap(Custcamera.bitmap2, 0, 0, Custcamera.bitmap2.getWidth(), Custcamera.bitmap2.getHeight(), matrix, true);

    }


    /*for baseline*/

    @Override
    protected void onResume() {
        super.onResume();

        //setStartCatcher();
       /* mSensorManager.registerListener(xyzAcc,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
        */
    }

    @Override
    protected void onPause() {

        super.onPause();
    }






    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        lati = location.getLatitude();
        longi = location.getLongitude();

        locationText.setText("Latitude: " + lati + "\nLongitude: " + longi);

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
        }catch(Exception e) {

        }

    }
    public void AddData(String newEntry){
        boolean insertData = mDatabaseHelper.addData(newEntry);
        if(insertData){
            toastMessage("Succesfully inserted");
        }
        else
        {
            toastMessage("Something went wrong");
        }
    }
    public void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 7 && resultCode == RESULT_OK) {

            bitmap = (Bitmap) data.getExtras().get("data");
            Images.cap_count++;
            switch(Images.cap_count){
                case 1:
                    ImageViewHolder.setImageBitmap(bitmap);
                    images.setImage_string_one(bitmap);
                    showVerbose(view);
                    break;
                case 2:
                    ImageViewHolder2.setVisibility(View.VISIBLE);
                    ImageViewHolder.getLayoutParams().height = 370;
                    ImageViewHolder2.getLayoutParams().height = 370;
                    ImageViewHolder2.setImageBitmap(bitmap);
                    images.setImage_string_two(bitmap);
                    hideCamera(view);
                    break;
                default:
                    Log.d("Camera error: ", "Not able to fetch image");
            }


        }
    }



    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(CLocation.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(CLocation.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }




    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(CLocation.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(CLocation.this,
                Manifest.permission.CAMERA))
        {

            // Printing toast message after enabling runtime permission.
            Toast.makeText(CLocation.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(CLocation.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);

        }
    }

    // Upload captured image online on server function.
    public void ImageUploadToServerFunction(){


        ByteArrayOutputStream byteArrayOutputStreamObject ;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        final String ConvertImage2 = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        CaptureImageFromCamera.setVisibility(View.VISIBLE);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                // Showing progress dialog at image upload time.
                progressDialog = ProgressDialog.show(CLocation.this,"Image is Uploading","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                Toast.makeText(CLocation.this,string1,Toast.LENGTH_LONG).show();

                // Setting image as transparent after done uploading.
                ImageViewHolder.setImageResource(android.R.color.transparent);


            }

            @Override
            protected String doInBackground(Void... params) {

                ImageProcessClass imageProcessClass = new ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                HashMapParams.put(ImageNameFieldOnServer, GetImageNameFromEditText);

                HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                HashMapParams.put("lati", String.valueOf(lati));

                HashMapParams.put("longi", String.valueOf(longi));

                HashMapParams.put("empid", String.valueOf(02));

                // HashMapParams.put(ImagePathFieldOnServer, ConvertImage);

                String FinalData = imageProcessClass.ImageHttpRequest(ImageUploadPathOnSever, HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);
                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);
                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(new OutputStreamWriter(OutPutStream, "UTF-8"));
                bufferedWriterObject.write(bufferedWriterDataFN(PData));
                bufferedWriterObject.flush();
                bufferedWriterObject.close();
                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){
                        stringBuilder.append(RC2);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;
            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilderObject.append("&");
                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilderObject.append("=");
                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }

    void showVerbose(View v){
        TextView verbose = findViewById(R.id.verbose);
        verbose.setText("Please move your camera 4 meters left");
    }

    void hideCamera(View v){
        TextView verbose = findViewById(R.id.verbose);
        verbose.setText("");
        CaptureImageFromCamera.setVisibility(View.INVISIBLE);
    }

}

