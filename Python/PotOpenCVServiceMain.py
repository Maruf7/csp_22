# -*- coding: utf-8 -*-
"""

"""
from threading import Thread
from CustomBM import MainProcess
from DB import DBManager
from time import sleep
import copy

class ProcessClientRequests(Thread):
    
    def __init__(self, dbmanager, RotateImageBy90 = False):
        Thread.__init__(self,name= "OpenCV python service for pothole measurement")
        self.parametersToProcess = []
        self.Run  = True
        self.Output = []
        self.dbmanager = dbmanager
        self.RotateImageBy90 = RotateImageBy90
        
        
    def addParameters(self, leftImage, rightImage, baseline_mm,focallength_mms, SensorSize_mm):
        self.parametersToProcess.append([leftImage, rightImage, baseline_mm, focallength_mms, SensorSize_mm])
        

        
    def run(self):
        print("[Info :] Starting service " + self.name + "...")
        self.Run  = True
        while(self.Run):
            records = self.dbmanager.getLatestData()
            if (len(records) > 0):
                print("[Info :] New records found processing...")
                for rt in records:
                  try:
                    leftImage1, rightImage1, baseline_mm1, focallength_mms1, SensorSize_mm1 = rt
                    ds = SensorSize_mm1.split('x')
                    if(len(ds) == 1):
                        sw = ds[0]
                        sh = sw
                    else:
                       sw, sh = ds
                    self.addParameters(leftImage1, rightImage1, float(baseline_mm1), float(focallength_mms1),
                                          [float(sw), float(sh)])
                  except Exception as e:
                      print("[Warning :] Skipping record due to incorrect fields " + str(e))
            else:
                print "."
            while(len(self.parametersToProcess) > 0):
              try:
                leftImage, rightImage, baseline_mm, focallength_mms, SensorSize_mm = self.parametersToProcess[0]
                print("Downloading images please wait..." + str([leftImage, rightImage]))
                lft = copy.copy(leftImage)
                #print(lft)
                del self.parametersToProcess[0]
                BBoxesDimensions, resultImage = MainProcess(leftImage,rightImage, baseline_mm, focallength_mms, SensorSize_mm,  self.RotateImageBy90,  isURL= True)
                print(lft)
                rs = [lft,rightImage, baseline_mm, focallength_mms, SensorSize_mm, BBoxesDimensions, resultImage]
                self.dbmanager.StoreResult(rs)
              except Exception as e:
                  print("[Error :]" + str(e))
            sleep(0.100)
          
    def __del__(self):
        self.Run = False
        self.join()
        del self.dbmanager
        print("[Info :] Stopping service " + self.name + "...")
        
if __name__ == '__main__':
    
    dbmanager = DBManager(host = "sql126.main-hosting.eu", user = "u911371405_maruf", passwd = "Qudsiya", db = "u911371405_csp", tableName="reports_two")
    service = ProcessClientRequests(dbmanager, RotateImageBy90 = False)
    service.start()
    
    while(True):
      try:
        #fetch records here
        #print ".",
        sleep(1)
      except KeyboardInterrupt:
         #close db connection
         break
    
    print("[Info :] Service closing...")
    del service

#that image should not have been roatted by 90.. make sure you put the flag right here... Have you reached?