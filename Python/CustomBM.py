from __future__ import print_function
import sys
from math import sqrt
from scipy import signal
import numpy as np
import cv2
import urllib

ResidedHeightImage = 400
baselineSet = 50 # in mm <--- Is this correct?yes sir its 5 to 6 cms.


currentMobile = None # cam you also now try with other set from same phone 3? ok sir i will
#currentMobile[0] = currentMobile[0]/4.0

def isclose(a, b, rtol=1e-05, atol=1e-08):
    return np.abs(a - b) <= (atol + rtol * np.abs(b))


def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)

    # return the resized image
    return resized


def stereo_match(leftOriginal, rightOriginal, kernel, max_offset_ratio):
    ReduceHeight = ResidedHeightImage
    # Load in both images, assumed to be RGBA 8bit per channel images
    #leftOriginal = cv2.imread(left_img)
    left = cv2.cvtColor(leftOriginal, cv2.COLOR_BGR2GRAY)  # Image.open(left_img).convert('L')
    left = image_resize(left, None, ReduceHeight)
    leftOriginal = image_resize(leftOriginal, None, ReduceHeight)

    # print(left[0:3,:].tolist())
    #rightOriginal = cv2.imread(right_img)
    right = cv2.cvtColor(rightOriginal, cv2.COLOR_BGR2GRAY)  # Image.open(right_img).convert('L')
    right = image_resize(right, None, ReduceHeight)
    rightOriginal = image_resize(rightOriginal, None, ReduceHeight)

    # print(right[0:3,:].tolist())
    h, w = left.shape  # left_img.size  # assume that both images are same size

    max_offset = int(max_offset_ratio * w)
    # Depth (or disparity) map
    depth = np.zeros((h, w), np.float32)
    # depth.shape = h, w

    kernel_half = int(kernel / 2)
    # print(kernel_half)
    # offset_adjust = 255 / max_offset  # this is used to map depth map output to 0-255 range

    if (False):
        wdw = int(h * 0.05)
        if (wdw % 2 == 0):
            wdw = wdw + 1
        left = cv2.blur(left.astype(np.uint8), (wdw, wdw))
        right = cv2.blur(right.astype(np.uint8), (wdw, wdw))

    for y in range(kernel_half, h - kernel_half, 1):
        print(str(int(y * 100.0 / (h - kernel_half))),
              end="\r")  # let the user know that something is happening (slowly!)

        for x in range(kernel_half, w - kernel_half, 1):
            window = right[y - kernel_half:y + kernel_half + 1, x - kernel_half:x + kernel_half + 1]
            window[window == 0] = 1
            window = np.reciprocal(window.astype(np.float32))
            # print(window.shape)
            if (x - max_offset < 0):
                xlr = 0
            else:
                xlr = x - max_offset
            if (x + max_offset > w):
                xhr = w - 1
            else:
                xhr = x + max_offset
            strip = left[y - kernel_half:y + kernel_half + 1, xlr:xhr].astype(np.float32)
            # print(strip.shape)
            grad = signal.convolve2d(strip, window, mode='valid')[0]
            indexes = np.array(range(xlr, xhr))
            # print(grad)
            # print("==========")
            # print(window.shape)
            grad = np.abs(grad - window.shape[0] * window.shape[1])  # kernel_half+1]
            # print(grad.tolist())
            minValue = grad.min()
            # print(minValue)
            if (minValue < 10.1):
                indexSort = np.argsort(grad)[:3]
                # print(indexSort)
                # print(grad[indexSort])
                # if(len(index[0]) >= 1):
                # print(indexes[indexSort])
                hdisp = np.abs(indexes[indexSort] - x)  # np.abs(index - grad.shape[1] / 2)
                # print(hdisp)
                # wdisp = np.abs(index[1]-x)
                # set depth output for this x,y location to the best match
                # depth[y, x] = best_offset * offset_adjust
                depth[
                    y, x] = hdisp.min()  # + wdisp.min()# sqrt((index[0][0] - y)*(index[0][0]-y) + (index[1][0]-x)*(index[1][0]-x))
                # if(depth[y,x] > max_offset):
                #      print("E")
            else:
                depth[y, x] = max_offset
                # print("-")

    # print(depth)
    dispMin = np.min(depth[depth > 0])
    maxDisp = np.max(depth)
    # print(dispMin)
    # print(maxDisp)
    dispDisplay = (depth - dispMin) / (maxDisp - dispMin) * 255.0
    # dispDisplay[dispDisplay==0] = 255.0
    dispDisplay = dispDisplay.astype(np.uint8)  # cv2.blur(dispDisplay.astype(np.uint8), (3, 3))
    # print(dispDisplay.tolist())
    # cv2.imshow('right', left)
    return leftOriginal, rightOriginal, depth, dispDisplay


def findDeepHoles(left, right, depth, dispDisplay):
    cv2.imshow('disparity', dispDisplay)
    maxDisp = np.max(depth)
    mtr = int(np.average(dispDisplay[dispDisplay <= 200]))
    print(mtr)
    # ret, threshDepth = cv2.threshold(dispDisplay, 10, mtr, cv2.THRESH_BINARY)
    threshDepth = np.zeros(dispDisplay.shape, dtype=np.uint8)
    threshDepth[np.logical_and(dispDisplay > 12, dispDisplay <= mtr)] = 255
    # threshDepth[threshDepth > mtr] = 0.0
    print(np.max(threshDepth))
    right = cv2.bitwise_and(right, right, mask=threshDepth)
    pitdepth = cv2.bitwise_and(depth, depth, mask=threshDepth)

    if (False):
        edges = cv2.Canny(left, 200, 100)
        cv2.imshow('edges', edges)
        ret, thresh = cv2.threshold(edges, 200, 255, 0)
        # thresh = cv2.bitwise_and(threshDepth, threshDepth)#, mask = thresh)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        deep = []
        for c in contours:
            if (len(c) > 0.25 * left.shape[1]):
                cv2.drawContours(left, [c], -1, (0, 255, 0), 2)

        cv2.imshow('thresh', thresh)
        cv2.imshow('left', left)
    # cv2.imshow('thresh', dispDisplay)
    cv2.imshow('Mask', threshDepth)
    cv2.imshow('right', right)
    # cv2.imwrite('disp.jpeg', dispDisplay)
    cv2.waitKey()
    cv2.destroyAllWindows()
    return pitdepth


def computeXYZ(pixel_index, ImageShape, disp, fx, baseline, units, cameraCCDWidth):
    # ImageResoutionWidth = widthImage#ResidedHeightImage
    pixelSize_mm = max(cameraCCDWidth) / max(ImageShape)
    ratioDisp = float(disp) / max(ImageShape)
    disp_Scale = int(ratioDisp * ImageShape[0])
    # print(disp_Scale)
    # if(disp_Scale > disp):
    #    disp = disp_Scale
    # print(pixelSize_mm)
    #print(fx)
    #print(baseline)
    #print(cameraCCDWidth)
    disp_mm = disp * pixelSize_mm
    z = (fx * baseline) / (units * disp_mm)
    #z = z/10.0
    zpxs = (4*fx * baseline) / (units * disp)

    if(ImageShape[0] > ImageShape[1]):
        x = (z / (fx))* (pixel_index[0] - ImageShape[0] / 2) *(max(cameraCCDWidth)/max(ImageShape))
        dy = float(pixel_index[1] - ImageShape[1] / 2) * ImageShape[0] / ImageShape[1]
        y = (z / fx )* dy *(max(cameraCCDWidth)/ImageShape[1])
    else:
        dx = (pixel_index[0] - ImageShape[0] / 2) * ImageShape[1] / ImageShape[0]
        x = (z / (fx)) * dx * (max(cameraCCDWidth) / ImageShape[0])
        dy = float(pixel_index[1] - ImageShape[1] / 2)
        y = (z / fx) * dy * (max(cameraCCDWidth) / max(ImageShape))
    x = (z / (fx)) * (pixel_index[0] - ImageShape[0] / 2) * (max(cameraCCDWidth) / max(ImageShape))
    y = (z / fx) * float(pixel_index[1] - ImageShape[1] / 2) * (max(cameraCCDWidth) / max(ImageShape))

    return x, y, z


def MergeRectsIfIntersect(A, B, pixelDistance=20):
    x = [A[0], A[0] + A[2], B[0], B[0] + B[2]]
    y = [A[1], A[1] + A[3], B[1], B[1] + B[3]]
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    if ((A[0] + A[2]) + pixelDistance < B[0] or A[0] - pixelDistance > (B[0] + B[2])):
        return None

    if ((A[1] + A[3]) + pixelDistance < B[1] or A[1] - pixelDistance > (B[1] + B[3])):
        return None

    return [xmin, ymin, xmax - xmin, ymax - ymin, (xmax - xmin)*(ymax - ymin)]


def PotholesGeneral(img, EdgeReductionfactor = 15):
    img = cv2.blur(img.astype(np.uint8), (5, 5))
    edges = cv2.Canny(img, 10 * EdgeReductionfactor, 5 * EdgeReductionfactor)
    cv2.imshow('edges', edges)
    images, contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    potholesDimension = []
    for c in contours:
        if (len(c) > 0.3 * min(img.shape)):
            cv2.drawContours(img, [c], -1, (0, 255, 0), 1)
            x, y, w, h = cv2.boundingRect(c)
            # cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),1)
            Added = False
            for i in range(0, len(potholesDimension)):
                rect = MergeRectsIfIntersect([x, y, w, h], potholesDimension[i], int(0.005 * max(img.shape)))
                if (rect is not None):
                    Added = True
                    potholesDimension[i] = rect
            if (Added == False):
                potholesDimension.append([x, y, w, h, w*h])

    cv2.imshow('contours', img)
    #print(potholesDimension)
    return potholesDimension


def Potholes(img):
    ReduceHeight = ResidedHeightImage
    # Load in both images, assumed to be RGBA 8bit per channel images
    # img = cv2.imread(img)
    # img = image_resize(img, None, ReduceHeight)
    img = cv2.blur(img.astype(np.uint8), (11, 11))
    factor = 4
    edges = cv2.Canny(img, 10 * factor, 5 * factor)
    cv2.imshow('edges', edges)  # oh the problem is the image is taken vertical do you have horizontal version?yes
    # circum = int(0.6*2*22.0*int(0.75*min(img.shape))/7.0)
    # print(circum)
    circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, 1, 50, param1=10, param2=50,
                               minRadius=int(0.35 * min(img.shape)), maxRadius=int(0.75 * max(img.shape)))

    potholesDimension = []
    if (circles is not None):
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            # cv2.circle(img,(i[0],i[1]),i[2],(255,0,0),2)
            # cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),1)
            potholesDimension.append([i[0] - i[2], i[1] - i[2], 2 * i[2], 2 * i[2], i[2]*i[2]])

    return potholesDimension


def AnnotatePotholes(rect, depth, img, floorDisparity, fx, baseline, units, cameraCCDWidth, showALL = False):
    y, x, h, w, area = rect
    #cv2.rectangle(img, (y, x), (y + h, x + w), (255, 255, 255), 1)
    if (showALL == False):
     if (w < 0.1 * depth.shape[1] or h < 0.1 * depth.shape[0]):
        print("[Warning :] Too small holes detected")
        return
    if (x + w > depth.shape[1]):
        w = depth.shape[1] - x-1

    if (y + h > depth.shape[0]):
        h = depth.shape[0] - y-1

    if (w < 5 or h < 5):
        return
    selectedDepth = depth[y:y + h, x:x + w]  # x depth[x:x+w, y:y+h]
    minDisp = np.average(selectedDepth[selectedDepth < np.average(selectedDepth[np.logical_and(selectedDepth > 0, selectedDepth < floorDisparity)])])
    isMinDispAboveFloorLevel = False
    if (showALL == False and  minDisp > floorDisparity):
        print("[Warning:] Found something above floor level")
        isMinDispAboveFloorLevel = True
        #return
    if(isMinDispAboveFloorLevel):
        maxDisp =  np.average(selectedDepth[selectedDepth > minDisp])
    else:
        maxDisp = min(floorDisparity, np.max(selectedDepth))

    if (minDisp > maxDisp):
        minDisp = np.min(selectedDepth)
        print("[Skipping :] Found something above floor pixel")
        return

    if((maxDisp - minDisp) < 0.005*depth.shape[1]):
        minDisp = np.min(selectedDepth[selectedDepth > 0])
        maxDisp = np.max(selectedDepth)
        print("[Alert :] Using extreme depth measurement " + str(maxDisp - minDisp))

    if (showALL == False and abs(maxDisp - minDisp) <= 0.02 * max(img.shape)):
        #cv2.rectangle(img, (y, x), (y + h, x + w), (255, 0, 0), 1)
        #print(str(maxDisp - minDisp))
        print("[Warning :] The disparity difference is too small")
        return

    if(True):
        xs, ys, ds1 = computeXYZ([0, 0], img.shape,  minDisp, fx, baseline, units, cameraCCDWidth)
        xs, ys, ds2 = computeXYZ([0, 0], img.shape, maxDisp, fx, baseline, units, cameraCCDWidth)
        ds = ds1 - ds2
    else:
        xs, ys, ds = computeXYZ([0, 0], img.shape, maxDisp - minDisp, fx, baseline, units, cameraCCDWidth)

    if (ds > 170):
        print("[Warning :] correcting depth " + str(ds))
        xs, ys, ds1 = computeXYZ([0, 0], img.shape, floorDisparity - minDisp, fx, baseline, units, cameraCCDWidth)
        xs, ys, ds2 = computeXYZ([0, 0], img.shape, maxDisp, fx, baseline, units, cameraCCDWidth)
        ds = ds1 - ds2

    if (showALL == False and ds < 20.0):
        cv2.rectangle(img, (y, x), (y + h, x + w), (255, 0, 0), 1)
        print("[Skipping :] The pothole depth is less than 2 cms")
        #return


    cv2.rectangle(img, (y, x), (y + h, x + w), (0, 255, 255), 1)
    #if (depth[y, x] < 2):
    #    depth[y, x] = maxDisp
    x1, y1, z1 = computeXYZ([y, x], img.shape, floorDisparity, fx, baseline, units, cameraCCDWidth)#depth[y, x])
    #if (depth[y + h, x + w] < 2):
    #    depth[y + h, x + w] = maxDisp
    x2, y2, z2 = computeXYZ([y + h, x + w], img.shape, floorDisparity, fx, baseline, units, cameraCCDWidth)# depth[y + h, x + w])
    width = abs(y2 - y1)
    height = abs(x2 - x1)
    if(showALL == False):
      if(width < 4 or height < 4):
        print("[Warning :] small pot hole detected")
        #return
    print("Depth of pothole found : " + str(ds) + "mm" + " (disparity = " + str(maxDisp - minDisp) + ")")
    print("Dimensions of pothole : " + str(int(width)) + "X" + str(int(height)) + "mm2")
    cv2.putText(img, str(int(height)) + "mm", (int(y + h / 2), x), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
    cv2.putText(img, str(int(width)) + "mm", (y, int(x + w / 2)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
    cv2.putText(img, str(int(ds)) + "mm", (int(y + h / 2), int(x + w / 2) + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
    return [rect, [ds, height, width]]

def removeCOmmonRectangles(rects):

    for i in range(0, len(rects)):
        for j in range(0, len(rects)):
          if(j > i and j< len(rects) and i < len(rects)):
            rect = MergeRectsIfIntersect(rects[i], rects[j], 5)
            if(rect is not None):
                #print("y")
                del rects[j]
                j = j-1

    return rects

#ref: https://www.pyimagesearch.com/2015/03/02/convert-url-to-image-with-python-and-opencv/
def url_to_image(url):
	# download the image, convert it to a NumPy array, and then read
	# it into OpenCV format
	resp = urllib.urlopen(url)
	image = np.asarray(bytearray(resp.read()), dtype="uint8")
	image = cv2.imdecode(image, cv2.IMREAD_COLOR)
 
	# return the image
	return image

def rotateImage90(img):
    num_rows, num_cols = img.shape[:2]

    rotation_matrix = cv2.getRotationMatrix2D((num_cols / 2, num_rows / 2), 90, 1)
    return cv2.warpAffine(img, rotation_matrix, (num_cols, num_rows))

def MainProcess(leftImage,rightImage, baseline_mm, focallength_mms, SensorSize_mm, rotateImgBy90 = False, isURL= False):
    
    if(isURL):
        leftImage = url_to_image(leftImage)
        rightImage = url_to_image(rightImage)
        print("[Info :] Download finished")
        if(leftImage is None):
            print("[Error :] Downloading the left image from url")
        if(rightImage is None):
            print("[Error :] Downloading the right image from url")
        

    if(baseline_mm < 10.0):
        baseline_mm = 40.0

    #focallength_mms = focallength_mms / 4.0

    if(rotateImgBy90 == True):
        print("[Warning: ] Rotating images by 90")
        leftImage = np.rot90(leftImage)
        rightImage = np.rot90(rightImage)


    MatchingBlockSize = 9  # block size >= 3 and can go high and it has to be odd number
    HowFarInImageWidthDoINeedToSearch = 0.25 #0.35 # range shoul be between 0 and 1 <-------------------last thing
    # left, right, depth, dispDisplay = stereo_match('pothole.jpg', 'pothole2.jpg', 5, int(ResidedHeightImage/2))
    # left, right, depth, dispDisplay = stereo_match('imgL.jpg', 'imgR.jpg', 7, int(ResidedHeightImage / 1.5))
    left, right, depth, dispDisplay = stereo_match(leftImage, rightImage, MatchingBlockSize,
                                                   HowFarInImageWidthDoINeedToSearch)
    floorDisparity = np.average(depth[depth < HowFarInImageWidthDoINeedToSearch * right.shape[1]])
    print("Floor disparity :" + str(floorDisparity))
    potholesDimensionPxs = PotholesGeneral(right, EdgeReductionfactor = 8) #<--------------------
    #print(potholesDimensionPxs)
    potholesDimensionPxs = sorted(potholesDimensionPxs, key=lambda row: row[4], reverse=True)
    potholesDimensionPxs = removeCOmmonRectangles(potholesDimensionPxs)
    print(potholesDimensionPxs)
    if (potholesDimensionPxs is None):
        return None
    else:
        result = []
        try:
            for p in potholesDimensionPxs:
              rt =  AnnotatePotholes(p, depth, right, floorDisparity, focallength_mms, baseline_mm, 1, SensorSize_mm,  showALL = False)
              if(rt is not None):
                  result.append(rt)
        except Exception as e:
             print("")
        cv2.imshow('disparity', dispDisplay)
        cv2.imshow('right', right)
        #cv2.imwrite('op.jpeg', right)
        ShowWIndows = True #<-----------------------------------
        if(ShowWIndows == True):
             cv2.waitKey()
        cv2.destroyAllWindows()
        #print(computeDisparity(50, depth.shape))
        return result, right
        #print(MergeRectsIfIntersect([10, 10, 10, 10], [7, 7, 10, 10]))

if __name__ == '__main__':
    leftImage = '22L.jpg'  #< if you chnage here then run python CustomBM.py ok?i changed the name of image should i run it from anaconada prompt?
    rightImage = '22R.jpg'  # try reversing images
    # leftImage = 'left.png'
    # rightImage = 'right.png'
    leftImage = cv2.imread(leftImage)
    rightImage = cv2.imread(rightImage)
    baseline = 60 #is this righjt? its 40
    focalLength = 3.37 #check these values in Android app that I had earlier refer to you...sir its correct!!
    SensorSize = [3.6368225, 2.7211492]#sir in android app CCDwidth is Showing 5.731462x3.2083127!! ok now run from anaconda prompt?correct always from anaconda prompt only!ok
    MainProcess(leftImage, rightImage, baseline, focalLength, SensorSize) #ok please change this as well ok sir

 # can i change parameters here  ?yes   done sir ok try now make depth field null yes sir not all but few show me
