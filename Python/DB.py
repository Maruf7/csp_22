# -*- coding: utf-8 -*-

import sys
import MySQLdb
import cv2

class DBManager:
    
    def __init__(self, host = "localhost", user = "root", passwd = "", db = "mastek", tableName="reports_two"):
        try:
           self.host = host
           self.user = user
           self.passwd = passwd
           self.db = db

           self.Initialised = False
           self.tableName = tableName
        except Exception as e:
            print("[Info :] Error connecting in Database "+ str(e))
            self.Initialised = False
            
    def ExecuteQuery(self, query, needsCommit= False):
        self.connection = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd, db=self.db)
        print("[Info :] Database connection established")
        cursor = self.connection.cursor()
        try:
          print("[Executing :] " + query)
          cursor.execute (query)
          data = cursor.fetchall()
          cursor.close()
        except Exception as e:
            print("[Info :] Error executing query " + query + " in Database "+ str(e))
            self.connection.close()
            return None
        if(needsCommit):
            self.connection.commit()
            print("[Info :] DB commited")
        self.connection.close()
        return data
    
    def __del__(self):
        if(self.Initialised == True):
            print("[Info : ] DB connection closing...")
            #self.connection.close()# sir actually we need to insert a row in database can you ask maruf wth the usecase// ok sir
        del self.connection
    
    def getLatestData(self):
        data =  self.ExecuteQuery("SELECT image_1,image_2, baseline, focallength, ccdwidth FROM " + self.tableName+ " WHERE depth IS NULL;")

        #self.ExecuteQuery("Mark all rows to processing")

        return data
    
    
    def ToString(self, *param):
        n = len(param)
        s = str(param[0])
        for i in range(1, n):
            s = s + ", " + str(param[i])
            
        return s
        
    
    def StoreResult(self, output):
        leftImage,rightImage, baseline_mm, focallength_mms, SensorSize_mm, BBoxesDimensions, resultImage = output
        try:
            if(len(SensorSize_mm) > 1):
              SensorSize_mm = str(SensorSize_mm[0]) + "x" +str(SensorSize_mm[1])
            maxPotholeDimension = BBoxesDimensions[0][1]
            maxPotholePositionOnImage =  BBoxesDimensions[0][0]
            depth_mm, height_mm, width_mm = maxPotholeDimension
            row, col, height, width, area = maxPotholePositionOnImage
            streamJpeg = "Not Stored" #cv2.imencode('jpg', resultImage)[1]#.tostring()
            values =  self.ToString(leftImage,rightImage, baseline_mm, focallength_mms, SensorSize_mm, depth_mm, height_mm, width_mm, row, col, height, width, streamJpeg)
            #self.ExecuteQuery("INSERT INTO " + self.tableName+ " VALUES ("+ values +");")
            self.ExecuteQuery("UPDATE  " + self.tableName + " SET depth ='" + str(depth_mm)+ "', height = '" + str(height_mm) +"', width='"+str(width_mm) +"' WHERE image_1='" + leftImage +"';", True)
        except Exception as e:
            depth_mm = height_mm = width_mm ="NA"
            self.ExecuteQuery(
                "UPDATE  " + self.tableName + " SET depth ='" + str(depth_mm) + "', height = '" + str(
                    height_mm) + "', width='" + str(width_mm) + "' WHERE image_1='" + leftImage + "';", True)

    

if __name__ == '__main__':
    dbmanager = DBManager(host = "sql126.main-hosting.eu", user = "u911371405_maruf", passwd = "Qudsiya", db = "u911371405_csp", tableName="reports_two")

    print(dbmanager.getLatestData())
    
    del dbmanager
